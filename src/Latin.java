public class Latin extends Languege{
    private final String alphabet = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
    private final int length = 4;
    private String word;
    public Latin(String newWord) throws Exception {
        if (checkAlphabet(newWord)){
            word = newWord;
        }
        else{
            throw new Exception("Слово не подходит под язык на латинице");
        }
    }
    @Override
    boolean checkAlphabet(String newWord) {
        int size = 0;
        if (newWord.length()==length) {
            for (int i = 0; i < newWord.length(); i++) {
                for (int j = 0; j < alphabet.length(); j++) {
                    if (newWord.charAt(i) == alphabet.charAt(j)) {
                        size++;
                        break;
                    }
                }
            }
        }
        else {
            return false;
        }
        if (size==newWord.length()){
            return true;
        }
        else {
            return false;
        }
    }
    String getWord() {
        return word;
    }
}

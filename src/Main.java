import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        dictionary();
    }
    public static void dictionary(){
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\\n");
        boolean doing = true;
        do {
            try {
                System.out.print("Введите название файла словаря: ");
                String file = sc.next();
                DictionarySomeToRus dictionarySTR = new DictionarySomeToRus(file);
                boolean menu = true;
                while (menu){
                    try {
                        System.out.println("1. Вывести словарь");
                        System.out.println("2. Получить значение по ключу");
                        System.out.println("3. Добавить новую пару");
                        System.out.println("4. Удалить запись по ключу");
                        System.out.println("0. Выход");
                        String choose = sc.next();
                        switch (choose) {
                            case ("1"):
                                dictionarySTR.OutputFullDictionary();
                                break;
                            case ("2"):
                                System.out.print("Введите ключ: ");
                                String key = sc.next();
                                dictionarySTR.foundValue(key);
                                break;
                            case ("3"):
                                System.out.print("Введите пару слов через пробел: ");
                                String keyAndValue = sc.next();
                                dictionarySTR.addKeyValue(keyAndValue);
                                break;
                            case ("4"):
                                System.out.print("Введите ключ: ");
                                String keyToDelete = sc.next();
                                dictionarySTR.deleteKeyValue(keyToDelete);
                                break;
                            case ("0"):
                                doing = false;
                                menu = false;
                                break;
                            default:
                                System.out.println("Выбран неправильный пункт меню. Попробуйте снова");
                                break;
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } while (doing);
    }
}
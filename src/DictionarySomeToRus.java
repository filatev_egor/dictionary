import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.*;

public class DictionarySomeToRus {
    private Map<Languege, Russian> dictionary;
    private String fileDirectory = "";
    private final String emptyDictionary = "Словарь пустой.";
    private final String noKey = "Такого слова на иностранном языке нет.";
    private final String keyAlreadyExist = "Перевод для этого слова уже есть.";
    private final String falseCombination = "Была введена не правильная комбинация ключа и значения.";
    private final String wasDeleted = "Запись была удалена.";
    private final String badCreate = "Одно или несколько из иностранных слов не соответсвуют условию.";

    private int count = 0;
    private boolean whichOne;
    private Languege word;


    public DictionarySomeToRus(String directory) throws Exception {
        Scanner scanner = new Scanner(new File(directory));
        fileDirectory = directory;
        ArrayList<String> dictianaryFull = new ArrayList<>();
        while (scanner.hasNextLine()) {
            dictianaryFull.add(scanner.nextLine());
        }
        scanner.close();
        dictionary = new HashMap<Languege, Russian>();
        for (int i=0; i<dictianaryFull.size(); i++){
            String[] splitDictionary = dictianaryFull.get(i).split("\\t+");
            count++;
            createNewDictionary(splitDictionary[0], splitDictionary[1]);
        }
    }

    public void OutputFullDictionary() throws Exception {
        if (!dictionary.isEmpty()){
            for(Map.Entry<Languege, Russian> entry : dictionary.entrySet()) {
                String key = entry.getKey().getWord();
                String value = entry.getValue().getWord();
                System.out.println(key + "\t" + value);
            }
        }
        else{
            throw new Exception(emptyDictionary);
        }
    }

    public void foundValue(String key) throws Exception {
        if (!dictionary.isEmpty()){
            if (findKeyByWord(key)){
                System.out.println(dictionary.get(word).getWord());
            }
            else{
                throw new Exception(noKey);
            }
        }
        else{
            throw new Exception(emptyDictionary);
        }
    }

    private void saveDictionary(){
        String dictionaryToString="";
        int countForSave =0;
        for(Map.Entry<Languege, Russian> entry : dictionary.entrySet()) {
            String key = entry.getKey().getWord();
            String value = entry.getValue().getWord();
            countForSave++;
            dictionaryToString+= key + "\t" + value;
            if (count!=countForSave){
                dictionaryToString+="\n";
            }
        }
        File file = new File(fileDirectory);
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(dictionaryToString);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void addKeyValue(String keyAndValue) throws Exception {
        try {
            String[] splitKeyAndValue = keyAndValue.split(" ");
            if (!dictionary.isEmpty()) {
                if (!findKeyByWord(splitKeyAndValue[0])) {
                    createNewWord(splitKeyAndValue[0]);
                    if (whichOne){
                        dictionary.put(new Latin(splitKeyAndValue[0]), new Russian(splitKeyAndValue[1]));
                    }
                    else{
                        dictionary.put(new NumberLanguage(splitKeyAndValue[0]), new Russian(splitKeyAndValue[1]));
                    }
                    count++;
                    saveDictionary();
                } else {
                    throw new Exception(keyAlreadyExist);
                }
            } else {
                    throw new Exception(emptyDictionary);
            }
        }
        catch (Exception e){
            throw new Exception(falseCombination);
        }
    }
    public void deleteKeyValue(String key) throws Exception {
        if (!dictionary.isEmpty()){
            if (findKeyByWord(key)){
                dictionary.remove(word);
                System.out.println(wasDeleted);
                saveDictionary();
            }
            else{
                throw new Exception(noKey);
            }
        }
        else{
            throw new Exception(emptyDictionary);
        }
    }
    private void createNewDictionary(String key, String value) throws Exception {
        if (count==1){
            if (key.length()==4){
                whichOne=true;
            }
            else if (key.length()==5){
                whichOne=false;
            }
            else{
                throw new Exception(badCreate);
            }
        }
        if (whichOne){
            dictionary.put(new Latin(key), new Russian(value));
        }
        else{
            dictionary.put(new NumberLanguage(key), new Russian(value));
        }
    }
    private void createNewWord(String key) throws Exception {
        if (whichOne){
            word = new Latin(key);
        }
        else{
            word = new NumberLanguage(key);
        }
    }
    private boolean findKeyByWord(String newWord) throws Exception {
        for (Map.Entry<Languege, Russian> entry : dictionary.entrySet()) {
            String key = entry.getKey().getWord();
            if (key.equals(newWord)){
                word = entry.getKey();
                return true;
            }
        }
        return false;
    }
}

public class Russian extends Languege{
    private final String alphabet = "ёЁйЙцЦуУкКеЕнНгГшШщЩзЗхХъЪфФыЫвВаАпПрРоОлЛдДжЖэЭяЯчЧсСмМиИтТьЬбБюЮ";
    private String word;
    public Russian(String newWord) throws Exception {
        if (checkAlphabet(newWord)){
            word = newWord;
        }
        else{
            throw new Exception("Слово не подходит под русский язык");
        }
    }
    @Override
    boolean checkAlphabet(String newWord) {
        int size = 0;
            for (int i = 0; i < newWord.length(); i++) {
                for (int j = 0; j < alphabet.length(); j++) {
                    if (newWord.charAt(i) == alphabet.charAt(j)) {
                        size++;
                        break;
                    }
                }
            }
        if (size==newWord.length()){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    String getWord() {
        return word;
    }
}

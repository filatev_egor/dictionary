public class NumberLanguage extends Languege{
    private final String alphabet = "1234567890";
    private final int length = 5;
    private String word;
    public NumberLanguage(String newWord) throws Exception {
        if (checkAlphabet(newWord)){
            word = newWord;
        }
        else{
            throw new Exception("Слово не подходит под язык на цифрах");
        }
    }
    @Override
    boolean checkAlphabet(String newWord) {
        int size = 0;
        if (newWord.length()==length) {
            for (int i = 0; i < newWord.length(); i++) {
                for (int j = 0; j < alphabet.length(); j++) {
                    if (newWord.charAt(i) == alphabet.charAt(j)) {
                        size++;
                        break;
                    }
                }
            }
        }
        else {
            return false;
        }
        if (size==newWord.length()){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    String getWord() {
        return word;
    }
}
